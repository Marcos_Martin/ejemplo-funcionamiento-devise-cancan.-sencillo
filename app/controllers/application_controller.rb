class ApplicationController < ActionController::Base
  protect_from_forgery

  rescue_from CanCan::AccessDenied do |exception|
    flash[:notice] = "Debes identificarte correctamente. Acceso denegado."
    session[:user_return_to] = request.url
    redirect_to root_path
  end

end
