#Dar roles a Cancan para poder interactuar con Devise
class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user

    if user.role? :administrator  #role de administrador con permiso para cualquier controlador
      can :manage, :all
    elsif user.role? :operator
      can :manage, :comprobar_operator #role de operador con permiso para el CRUD del controlador ComprobarOperator(comprobar_operator_controller.rb)
      can [:index], :comprobar_invitado #tambien puede pasar al index de ComprobarInvitado#index
    else
      can [:index], :comprobar_invitado             #role usuario invitado (solo puede acceder a ComprobarInvitado#index)
    end
  end
end
#can puede tener  :create, :read, :update, :delete, and :manage por defecto. Tambien puedes crear los tuyos. Mirar documentacion CanCan
#otra manera de crear inicialize. =begin / =end son bloque de comentarios en ruby
=begin
  def initialize(user)
    can :manage, :all if user.is_administrator?
  end
=end