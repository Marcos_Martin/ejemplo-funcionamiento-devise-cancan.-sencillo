EjemploDeviseCancan::Application.routes.draw do
  #get "home/index"

  #match '/' => 'home#index', :as => :root
  devise_for :users

  resources :users
  resources :roles

  match 'comprobar_invitado'=> 'comprobar_invitado#index', :as=> 'comprobar_invitado'
  match 'comprobar_admin'=> 'comprobar_admin#index', :as=> 'comprobar_admin'
  match 'comprobar_operator'=> 'comprobar_operator#index', :as=> 'comprobar_operator'
  root to: 'home#index'




end
