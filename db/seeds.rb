# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Role.create( :name => "Administrator"){ |c| c.id = 1 }.save

User.create(:email => 'admin@fehrcarem.com', :password => 'Fehrcarem01', :password_confirmation => "Fehrcarem01" ){ |c| c.roles << Role.first }.save
